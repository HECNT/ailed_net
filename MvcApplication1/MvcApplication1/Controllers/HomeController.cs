﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Modifique esta plantilla para poner en marcha su aplicación ASP.NET MVC.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Página de descripción de la aplicación.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Página de contacto.";

            return View();
        }

        public ActionResult test()
        {

            return View();
        }

        public ActionResult Login()
        {
            return View();
        }
        
        public ActionResult login_admin()
        {
            return View();
        }

        public ActionResult A2()
        {
            return View();
        }

        public ActionResult A3()
        {
            return View();
        }

        public ActionResult A51()
        {
            return View();
        }

        public ActionResult A61()
        {
            return View();
        }
    }
}
